// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var minifyCSS = require('gulp-minify-css');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// COMPASS, THEN MINIFY CSS
gulp.task('compass', function() {
    console.log("Starting Compass compile");
    gulp.src('./assets/sass/*.scss')
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(compass({
            css: './assets/css',
            sass: './assets/sass',
            image: './assets/images'
        }))
        .on('error', function(err) {
            // Would like to catch the error here
            console.log(err.messageOriginal);
        })
        .pipe(minifyCSS())
        .pipe(gulp.dest('./public/'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['./vendor/*.js', './assets/js/*.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./public/'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('./assets/js/*.js', ['lint', 'scripts']);
    gulp.watch('./assets/sass/*.scss', ['compass']);
});

// Default Task
gulp.task('default', ['lint', 'scripts', 'watch']);