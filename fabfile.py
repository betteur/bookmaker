from fabric.api import *

env.hosts = [
   'preprod.betteur.com',
    'www.betteur.com'
]

env.user = "root"

def deploy():
    with cd('/var/discourse/pages/bookmakers'):
        run('git pull origin master')
