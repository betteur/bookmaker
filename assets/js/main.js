$(document).ready(function() {
// Check if mobile is present.
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if (isMobile.any()) {
        $('#betclic-link').attr('href', 'https://www.betteur.com/betclic/register/mobile');
        $('#netbet-link').attr('href', 'https://www.betteur.com/netbet/register/mobile');
        $('#france-pari-link').attr('href', 'https://www.betteur.com/france-pari/register/mobile');
        $('#bwin-link').attr('href', 'https://www.betteur.com/bwin/register/mobile');
        $('#fdj-link').attr('href', 'https://www.betteur.com/parionsweb/register/mobile');
        $('#unibet-link').attr('href', 'https://www.betteur.com/unibet/register/mobile');
        $('#zebet-link').attr('href', 'https://www.betteur.com/zebet/register/mobile');
        $('#winamax-link').attr('href', 'https://www.betteur.com/winamax/register/mobile');
        $('#pmu-link').attr('href', 'https://www.betteur.com/pmu/register/mobile');
    } else {
        $('#betclic-link').attr('href', 'https://www.betteur.com/betclic/register/desktop');
        $('#netbet-link').attr('href', 'https://www.betteur.com/netbet/register/desktop');
        $('#france-pari-link').attr('href', 'https://www.betteur.com/france-pari/register/desktop');
        $('#bwin-link').attr('href', 'https://www.betteur.com/bwin/register/desktop');
        $('#fdj-link').attr('href', 'https://www.betteur.com/parionsweb/register/desktop');
        $('#unibet-link').attr('href', 'https://www.betteur.com/unibet/register/desktop');
        $('#zebet-link').attr('href', 'https://www.betteur.com/zebet/register/desktop');
        $('#winamax-link').attr('href', 'https://www.betteur.com/winamax/register/desktop');
        $('#pmu-link').attr('href', 'https://www.betteur.com/pmu/register/desktop');
    }

});
